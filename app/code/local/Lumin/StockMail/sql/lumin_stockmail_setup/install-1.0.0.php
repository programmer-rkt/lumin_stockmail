<?php
/**
 * Lumin_StockMail Magento Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Closed Source License. It uses a custom
 * license which applied some restriction for the reuse of this extension. The
 * license copy is bundled with this package in the file LICENSE_Lumin_StockMail.txt.
 *
 * If you did not receive a copy of the license, then  please send an email to
 * rajeevphpdeveloper@gmail.com so that I can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future. If you wish to customize this extension or wish
 * to add new features to this extension, then youc can contact me for that.
 *
 * @category  Lumin
 * @package   Lumin_StockMail
 * @author    Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 * @copyright Copyright (c) 2017 Rajeev K Tomy
 * @license   Closed Source License. read : LICENSE_Lumin_StockMail.txt
 */

/**
 * @var Lumin_StockMail_Model_Resource_Setup $this
 */
$installer = $this;

$installer->startSetup();

//add attributes to catalog product
$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'notification_address',
    array(
        'group'                      => 'General',
        'input'                      => 'textarea',
        'sort_order'                 => 100,
        'type'                       => 'text',
        'label'                      => 'Notification Address',
        'backend'                    => '',
        'visible'                    => true,
        'required'                   => false,
        'user_defined'               => false,
        'searchable'                 => false,
        'filterable'                 => false,
        'comparable'                 => false,
        'visible_on_front'           => false,
        'visible_in_advanced_search' => false,
        'is_html_allowed_on_front'   => false,
        'backend'                    => 'lumin_stockmail/product_attribute_backend_notifyaddress',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'                       => 'Comma separated notification email address list',
    )
);

$installer->endSetup();
