<?php
/**
 * Lumin_StockMail Magento Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Closed Source License. It uses a custom
 * license which applied some restriction for the reuse of this extension. The
 * license copy is bundled with this package in the file LICENSE_Lumin_StockMail.txt.
 *
 * If you did not receive a copy of the license, then  please send an email to
 * rajeevphpdeveloper@gmail.com so that I can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future. If you wish to customize this extension or wish
 * to add new features to this extension, then youc can contact me for that.
 *
 * @category  Lumin
 * @package   Lumin_StockMail
 * @author    Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 * @copyright Copyright (c) 2017 Rajeev K Tomy
 * @license   Closed Source License. read : LICENSE_Lumin_StockMail.txt
 */

/**
 * Default Helper Class
 *
 * @category Lumin
 * @package  Lumin_StockMail
 * @author   Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 */
class Lumin_StockMail_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_NOTIFICATION_EMAIL_TEMPLATE = 'lumin_stockmail/general/email_notification_template';
    const XML_PATH_TO_NAME_IDENTIY             = 'lumin_stockmail/general/to_address_name';
    const XML_PATH_FROM_EMAIL_IDENTITY         = 'lumin_stockmail/general/from_adress_email';
    const XML_PATH_FROM_NAME_IDENTITY          = 'lumin_stockmail/general/from_adress_name';

    /**
     * Send out of stock mail
     *
     * @param  Mage_Catalog_Model_Product $product
     * @param  mixed int|string           $storeId
     * @return $this
     */
    public function sendNotificationMail(Mage_Catalog_Model_Product $product, $storeId)
    {
        $mailer         = Mage::getModel('core/email_template_mailer');
        $templateParams = array('product' => $product);
        $sender         = $this->getConfigFromMail($storeId); //array
        $toName         = $this->getConfigToMailName($storeId);
        $templateId     = $this->_getConfig(
            self::XML_PATH_NOTIFICATION_EMAIL_TEMPLATE,
            $storeId
        );
        $notificationMails = explode(',', $product->getNotificationAddress());
        foreach ($notificationMails as $email) {
            $toMail    = trim($email);
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($toMail, $toName);
            $mailer->addEmailInfo($emailInfo);
            $mailer->setSender($sender);
            $mailer->setStoreId($storeId);
            $mailer->setTemplateId($templateId);
            $mailer->setTemplateParams($templateParams);
            $mailer->send();
        }
        return $this;
    }

    /**
     * From mail details
     *
     * @param  mixed int|string $storeId
     * @param  string           $param
     * @return array            $sender
     */
    public function getConfigFromMail($storeId)
    {
        $nameId = self::XML_PATH_FROM_NAME_IDENTITY;
        $mailId = self::XML_PATH_FROM_EMAIL_IDENTITY;
        $sender = array(
            'name'  => $this->_getConfig($nameId, $storeId),
            'email' => $this->_getConfig($mailId, $storeId),
        );
        return $sender;
    }

    /**
     * To address name
     *
     * @param  mixed int|string $storeId
     * @return string
     */
    public function getConfigToMailName($storeId)
    {
        return $this->_getConfig(self::XML_PATH_FROM_NAME_IDENTITY, $storeId);
    }

    /**
     * Short for Mage::getStoreConfig()
     *
     * @param  String  $path
     * @param  String | Int $storeId
     * @return String | Null
     */
    protected function _getConfig($path, $storeId)
    {
        return Mage::getStoreConfig($path, $storeId);
    }
}
