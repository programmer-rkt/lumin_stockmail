<?php
/**
 * Lumin_StockMail Magento Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Closed Source License. It uses a custom
 * license which applied some restriction for the reuse of this extension. The
 * license copy is bundled with this package in the file LICENSE_Lumin_StockMail.txt.
 *
 * If you did not receive a copy of the license, then  please send an email to
 * rajeevphpdeveloper@gmail.com so that I can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future. If you wish to customize this extension or wish
 * to add new features to this extension, then youc can contact me for that.
 *
 * @category  Lumin
 * @package   Lumin_StockMail
 * @author    Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 * @copyright Copyright (c) 2017 Rajeev K Tomy
 * @license   Closed Source License. read : LICENSE_Lumin_StockMail.txt
 */

/**
 * Observer class.
 *
 * @category Lumin
 * @package  Lumin_StockMail
 * @author   Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 */
class Lumin_StockMail_Model_Observer
{

    /**
     * Send notification mail if the inventory is out of stock
     *
     * @param  Varien_Event_Observer $observer
     * @return $this
     */
    public function inventoryStockItemCorrectAfter(Varien_Event_Observer $observer)
    {
        /**
         * @var $items array
         * format: array($productId => array('qty'=>$qty, 'item'=>$stockItem))
         */
        $items = $observer->getItems();
        foreach ($items as $item) {
            $stockItem = $item['item'];
            $stockQty  = $item['qty'];
            if ($stockItem->getQty() - $stockItem->getMinQty() - $stockQty <= 0) {
                $product = Mage::getModel('catalog/product')
                    ->load($stockItem->getProductId());
                $storeId = $stockItem->getStoreId();
                Mage::helper('lumin_stockmail')
                    ->sendNotificationMail($product, $storeId);
            }
        }
        return $this;
    }

    public function adminhtmlCatalogProductSaveCommitAfter(Varien_Event_Observer $observer)
    {
        $product = $observer->getProduct();
        $stockData = $product->getStockItem()->getData();
        if (isset($stockData['qty']) && $stockData['qty'] == 0) {
            $storeId = $product->getStoreId();
            Mage::helper('lumin_stockmail')
                ->sendNotificationMail($product, $storeId);
        }
        return $this;
    }
}