<?php
/**
 * Lumin_StockMail Magento Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Closed Source License. It uses a custom
 * license which applied some restriction for the reuse of this extension. The
 * license copy is bundled with this package in the file LICENSE_Lumin_StockMail.txt.
 *
 * If you did not receive a copy of the license, then  please send an email to
 * rajeevphpdeveloper@gmail.com so that I can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future. If you wish to customize this extension or wish
 * to add new features to this extension, then youc can contact me for that.
 *
 * @category  Lumin
 * @package   Lumin_StockMail
 * @author    Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 * @copyright Copyright (c) 2017 Rajeev K Tomy
 * @license   Closed Source License. read : LICENSE_Lumin_StockMail.txt
 */

/**
 * Add events to observe stock qty change
 *
 * We cannot rely on Mage_CatalogInventory module to look for the
 * inventory change, since Magento uses optimized queries to update
 * catalog inventory, which prevents us to use a single event to do
 * the job.
 *
 * Otherwise, we need to listen to multiple events and trigger the mail
 * which I believe make this module much more complex.
 *
 * So this rewrite would remove all such chaos. Peace.
 *
 * @category Lumin
 * @package  Lumin_StockMail
 * @author   Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 */
class Lumin_StockMail_Model_CatalogInventory_Stock extends Mage_CatalogInventory_Model_Stock
{
    const EVENT_CORRECT_STOCK_ITEMS_QTY_BEFORE = 'cataloginventory_stock_item_correct_qty_before';
    const EVENT_CORRECT_STOCK_ITEMS_QTY_AFTER = 'cataloginventory_stock_item_correct_qty_after';

    /**
     * (non-PHPdoc)
     * @see Mage_CatalogInventory_Model_Stock::registerProductsSale()
     */
    public function registerProductsSale($items)
    {
        Mage::dispatchEvent(self::EVENT_CORRECT_STOCK_ITEMS_QTY_BEFORE, array(
            'stock'     => $this,
            'items_obj' => (object)array('items' => &$items),
            'operator'  => '+'
        ));
        $result = parent::registerProductsSale($items);
        Mage::dispatchEvent(self::EVENT_CORRECT_STOCK_ITEMS_QTY_AFTER, array(
            'stock'          => $this,
            'items'          => $items,
            'fullsave_items' => $result,
            'operator'       => '+'
        ));
        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Mage_CatalogInventory_Model_Stock::revertProductsSale()
     */
    public function revertProductsSale($items)
    {
        Mage::dispatchEvent(self::EVENT_CORRECT_STOCK_ITEMS_QTY_BEFORE, array(
            'stock'     => $this,
            'items_obj' => (object)array('items' => &$items),
            'operator'  => '-'
        ));
        $result = parent::revertProductsSale($items);
        Mage::dispatchEvent(self::EVENT_CORRECT_STOCK_ITEMS_QTY_AFTER, array(
            'stock'          => $this,
            'items'          => $items,
            'fullsave_items' => $result,
            'operator'       => '-'
        ));
        return $result;
    }
}
