#Lumin_StockMail Magento Extension

This will send notification mails to those email address which are set at product level when the product become out of stock.

##Features

- Custom notification template comes with this extension. If you want, there is a provision to change the cutom notification email template from the system config section.
- You can change the To address name as you need from system configuration
- You can configure from address (both name and mail address) from system configuration.
- Mail will be triggered whenever there the inventory become zero.
